package com.teamC.Challenge3.model;

import java.util.*;

/**
 * Implentasi OperationInterface
 */
public class Operation implements OperationInterface {

    private double mean;
    private double median;
    private int mode;

    // Constructor Operation
    public Operation(List<Integer> data) {
        this.htgMean(data);
        this.htgMedian(data);
        this.htgMode(data);
    }

    // Getter Method
    public double getMean() {
        return mean;
    }

    public int getMode() {

        return mode;
    }

    public double getMedian() {

        return median;
    }

    // Setter Method
    /***
     * Implementasi Stream
     */
    public void htgMean(List<Integer> m) {
        this.mean = m.stream()
            .mapToDouble(opt -> opt)
            .average()
            .orElse(0.0);
    }

    // 2. method htgMode (Positive Negative Test) Titi
    public void htgMode(List<Integer> m) {
        int n = m.size();
        int maxValue = 0, maxCount = 0, i, j;
        Optional<List<Integer>> isNull = Optional.ofNullable(m);
        if (isNull.isPresent()) {
            for (i = 0; i < n; ++i) {
                int count = 0;
                for (j = 0; j < n; ++j) {
                    if (m.get(j) == m.get(i))
                        ++count;
                }
                if (count > maxCount) {
                    maxCount = count;
                    maxValue = m.get(i);
                }
            }
        this.mode = maxValue;
        } else
            System.out.println("List is empty");
    }

    public void htgMedian(List<Integer> m) {
        double nTengah;
        Optional<List<Integer>> isNull = Optional.ofNullable(m);
        if (isNull.isPresent()) {
            if (m.size() % 2 == 1)
                nTengah = m.get(m.size() / 2);
            else
                nTengah = ((double) (m.get(m.size() / 2) + m.get((m.size() / 2) - 1))) / 2;
            this.median = nTengah;
        } else{
            throw new NullPointerException();
        }
    }

}