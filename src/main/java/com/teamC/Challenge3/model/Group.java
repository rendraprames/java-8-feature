package com.teamC.Challenge3.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Implementasi GroupInterface
 */

public class Group implements GroupInterface {

    /**
     * Implementasi HashMap dari Java Collection
     */

    private Map<Integer, Integer> grupLebihDari = new HashMap<>();
    private Map<Integer, Integer> grupKurangDari = new HashMap<>();
    private Map<Integer, Integer> grupTengah = new HashMap<>();
    private HashMap<Integer, Integer> mapTemp = new HashMap<>();

    // Constructor Group
    public Group(List<Integer> data, int pembatas) {
        this.setNilaiLebihDari(data,pembatas);
        this.setNilaiKurangDari(data,pembatas);
        this.setNilaiTengah(data,pembatas);
    }

    // Getter Method
    public Map<Integer, Integer> getNilaiLebihDari(){
        return grupLebihDari;
    }

    public Map<Integer, Integer> getNilaiKurangDari() {

        return grupKurangDari;
    }
    public Map<Integer, Integer> getNilaiTengah() {

        return grupTengah;
    }

    public void setMap(List<Integer> data) {
        for (Integer nilai : data) {
            int count = 0;
            for (Integer nilai2 : data) {
                if (Objects.equals(nilai2, nilai)) {
                    ++count;
                }
            }
            this.mapTemp.put(nilai, count);

        }
    }

    public void setNilaiLebihDari(List<Integer> data, int pembatas){
        setMap(data);
        Map<Integer,Integer> collect = mapTemp.entrySet().stream()
                .filter(x -> x.getKey() > pembatas)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        this.grupLebihDari = collect;
    }

    public void setNilaiKurangDari(List<Integer> data, int pembatas){
        setMap(data);
        Map<Integer,Integer> collect = mapTemp.entrySet().stream()
                .filter(x -> x.getKey() < pembatas)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        this.grupKurangDari = collect;
    }

    public void setNilaiTengah(List<Integer> data, int pembatas){
        setMap(data);
        Map<Integer,Integer> collect = mapTemp.entrySet().stream()
                .filter(x -> x.getKey() == pembatas)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        this.grupTengah = collect;
    }

}