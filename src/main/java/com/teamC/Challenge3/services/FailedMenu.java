package com.teamC.Challenge3.services;

import java.util.Scanner;

/**
 * Implementasi Inhearitance dari MainMenu Class
 */
public class FailedMenu extends com.teamC.Challenge3.services.MainMenu {

    /**
     * Implementasi Polymorhfing dari method tampil(Overriding)
     */
    @Override
    public void tampil() {
        com.teamC.Challenge3.services.MainMenu menu = new com.teamC.Challenge3.services.MainMenu();
        Scanner input = new Scanner(System.in);
        // input.close();
        System.out.println("=============================");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("=============================");

        System.out.println("File tidak ditemukan");
        System.out.println("0. Exit");
        System.out.println("1. kembali ke menu utama");
        System.out.println("Silahkan pilih menu");
        int pilih = input.nextInt();
        switch (pilih){
            case 0 : {
                System.out.println("Program SELESAI");
                System.exit(0);
                break;
            }
            case 1 : menu.tampil();
            break;
            default : {
                System.out.println("Pilihan tidak ditemukan, silahkan pilih lagi");
                this.tampil();
            }
        }
    }
}
