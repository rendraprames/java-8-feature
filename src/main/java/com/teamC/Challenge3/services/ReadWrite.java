package com.teamC.Challenge3.services;

import com.teamC.Challenge3.model.*;

import java.io.*;
import java.util.*;

/**
 * Implementasi Interface dari ReadWriteInterface Class
 */
public class ReadWrite implements ReadWriteInterafce {

    public final List<Integer> Data_Nilai = new ArrayList<>();
    private final String HASIL = "FinalFile";
    FailedMenu menu = new FailedMenu();

    // Constructor method
    public ReadWrite() {
        File createDirectory = new File(HASIL);
        if (createDirectory.mkdir()) {
            System.out.println("Folder" + HASIL + "berhasil di cetak");
        }
        String fileName = "data_sekolah.csv";
        List<List<String>> data = this.readFIle(fileName, ";");
        for (List<String> dataTiapKelas : data) {
            dataTiapKelas.remove(0);
            for (int j = 0; j < dataTiapKelas.size(); j++) {
                Data_Nilai.add(Integer.valueOf(dataTiapKelas.get(j)));
            }
        }
        //method reference
        Collections.sort(Data_Nilai, Integer::compareTo);
    }

    /**
     * Implementasi dari Java Standard Class (Writing)
     *
     */
    public void generateOperation(String txtFile){
        Operation oper = new Operation(Data_Nilai);

        try {
            File file = new File(HASIL + "/" + txtFile);
            if (file.createNewFile()){
                System.out.println("New file is created");
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("Hasil Perhitungan Data: ");
            bwr.newLine();
            bwr.newLine();
            bwr.write("Nilai Mean\t\t: " + oper.getMean());
            bwr.newLine();
            bwr.write("Nilai Median\t: " + oper.getMedian());
            bwr.newLine();
            bwr.write("Nilai Mode\t\t: " + oper.getMode());
            bwr.newLine();
            bwr.flush();
            bwr.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void generateGroup(String txtFile, int delimiter){
        Group grup = new Group(Data_Nilai, delimiter);
        try {
            File file = new File(HASIL + "/" + txtFile);
            if (file.createNewFile()){
                System.out.println("New file is created");
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("Hasil Pengelompokan Data : ");
            bwr.newLine();
            bwr.newLine();
            bwr.write("Nilai data kurang dari "+ delimiter+" :");
            bwr.newLine();
            HashMap<Integer, Integer> kurang = new HashMap<>(grup.getNilaiKurangDari());
            kurang.forEach((key2, value2) -> {
                try {
                    bwr.write("Nilai " + key2 + " berjumlah " +value2);
                    bwr.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            bwr.newLine();

            HashMap<Integer, Integer> tengah = new HashMap<>(grup.getNilaiTengah());
            tengah.forEach((key3, value3) -> {
                try {
                    bwr.write("Nilai " + key3 + " berjumlah " +value3);
                    bwr.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            bwr.newLine();
            bwr.write("Nilai data lebih dari "+ delimiter+" :");
            bwr.newLine();

            HashMap<Integer, Integer> jumlah = new HashMap<>(grup.getNilaiLebihDari());
            HashMap<Integer, Integer> jumlahSort = new HashMap<>(jumlah);
            jumlahSort.forEach((key, value) -> {
                try {
                    bwr.write("Nilai " + key + " berjumlah " +value);
                    bwr.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            bwr.newLine();
            bwr.flush();
            bwr.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Implementasi dari Java Standard Class (Reading)
     */
    public List<List<String>> readFIle(String fileName, String delimiter){
        try{
            File file = new File(fileName);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;
            List<String>innerList;
            List<List<String>> outerList = new ArrayList<>();
            while ((line=br.readLine())!=null){
                tempArr = line.split(delimiter);
                innerList = new ArrayList<>(Arrays.asList(tempArr));
                outerList.add(innerList);
            }
            br.close();
            return outerList;
        }catch (Exception e){
            menu.tampil();
            return null;
        }
    }
}
