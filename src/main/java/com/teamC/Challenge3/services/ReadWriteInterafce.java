package com.teamC.Challenge3.services;

import com.teamC.Challenge3.Challenge3Application;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.util.List;

interface ReadWriteInterafce {

    void generateOperation(String txtFile);

    void generateGroup(String txtFile, int delimiter);

    List<List<String>> readFIle(String fileName, String delimiter);

//    class ServletInitializer extends SpringBootServletInitializer {
//
//        @Override
//        protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//            return application.sources(Challenge3Application.class);
//        }
//
//    }
}
