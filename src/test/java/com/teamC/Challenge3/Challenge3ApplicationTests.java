package com.teamC.Challenge3;

import org.junit.jupiter.api.Assertions;
import com.teamC.Challenge3.model.Operation;
import com.teamC.Challenge3.services.ReadWrite;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.*;

@SpringBootTest
class Challenge3ApplicationTests {
	private final ReadWrite readWrite = new ReadWrite();
	private final Operation operation = new Operation(readWrite.Data_Nilai);
	private final List<Integer> example =  List.of(1,2,3 ,3, 4,5);

	/***
	 * Test the htgMean() method with positive case
	 */
	@Test
	@DisplayName("Positive Test - htgMean() @ Operation Class")
	void testHtgMean() {
		operation.htgMean(example);
		Double res = operation.getMean();
		Assertions.assertEquals(3, res);
	}

	@Test
	@DisplayName("Negative Test - htgMean() @ Operation Class | Null Pointer Exception")
	void testMeanNullPointer() {
		assertThrows(NullPointerException.class, () -> operation.htgMean(null));
	}

	/*
	 * Positive and Negative Test Unit Median added by Rendra
	 */
	@Test
	@DisplayName("Positive Test - Success")
	public void testMedianSuccess(){
		operation.htgMedian(example);
		double result = operation.getMedian();
		assertEquals(3, result);
	}

	@Test
	@DisplayName("Negative Test - Failed Null Pointer")
	public void testMedianFailed(){
		assertThrows(NullPointerException.class, () -> operation.htgMedian(null));
	}

	/***
	 * Test htgMode() method with positive case
	 */
	@Test
	@DisplayName("Positive Test - Success")
	void htgMode() {
		operation.htgMode(example);
		int res = operation.getMode();
		assertEquals(3, res);
	}

	/***
	 * Test htgMode() method with negative case
	 */
	@Test
	@DisplayName("Negative Test - Null Pointer Exception")
	void htgModeNullPointer() {
		assertThrows(NullPointerException.class, () -> operation.htgMode(null));

	}
}
